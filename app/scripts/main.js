'use strict';

$(document).ready(function(){
	$('#scroll-home').click(function(e){
		e.preventDefault();
		$('body').animate({scrollTop: $('#top').offset().top }, 700);
	});

	$('#scroll-us').on('click', function(e){
		e.preventDefault();
		$('html,body').animate({
			scrollTop: $('#us').offset().top
		}, 700);
	});

	$('#scroll-services').on('click',function(e){
		e.preventDefault();
		$('html,body').animate({scrollTop: $('#services').offset().top}, 700);
	});

	$('#scroll-applications').click(function(e){
		e.preventDefault();
		$('html,body').animate({scrollTop: $('#applications').offset().top}, 700);
	});

	$('#scroll-team').click(function(e){
		e.preventDefault();
		$('html,body').animate({scrollTop: $('#team').offset().top}, 700);
	});

	$('#scroll-arrow').click(function(e){
		e.preventDefault();
		$('html,body').animate({scrollTop: $('#us').offset().top}, 700);
	});

/*	$(window).on('scroll', function(){
		var dividerUs = $('#divider-us');
    //var dividerWhoWeAre = $('#divider-who-we-are');
    if ((dividerUs.offset().top - $(this).scrollTop()) <= 135 || (dividerUs.offset().top-$(this).scrollTop()) <= ($(this).height()/2)) {
      showWhoWeAre();
    }
	});*/
});

/*function showWhoWeAre(){
	$('#who-we-are-text').removeClass('invisible');
	$('#who-we-are-img').removeClass('invisible');

	$('#who-we-are-text').addClass('animated slideInLeft');
	$('#who-we-are-img').addClass('animated slideInRight');
}*/
