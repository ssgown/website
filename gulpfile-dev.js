'use strict';

var gulp = require('gulp');
var connect = require('gulp-connect');
var jshint = require('gulp-jshint');
var inject = require('gulp-inject');
var wiredep = require('wiredep').stream;
var less = require('gulp-less');
var path = require('path');

var sources = {
    html : [
        './app/index.html'
    ],
    stylesheets: [
        './app/stylesheets/css/**/*.css',
        './app/stylesheets/css/bootstrap/bootstrap-readable.min.css'
    ],
    scripts: [
        './gulpfile.js',
        './app/scripts/*.js'
    ]
};

gulp.task('server',function(){
    connect.server({
        root:'./app',
        port: 3000,
        hostname: '0.0.0.0',
        livereload: true
    });
});

gulp.task('html',function(){
    gulp.src(sources.html)
    .pipe(connect.reload());
});

gulp.task('css',function(){
    gulp.src(sources.stylesheets)
    .pipe(connect.reload());
});

gulp.task('less', function () {
  //return gulp.src('./app/lib/bootstrap/less/**/*.less')
  return gulp.src('./app/stylesheets/less/**/*.less')
    .pipe(less({
      paths: [ path.join(__dirname, 'less', 'includes') ]
    }))
    .pipe(gulp.dest('./app/stylesheets/css'));
});

gulp.task('jshint',function(){
    return gulp.src(sources.scripts)
    .pipe(jshint('.jshintrc'))
    .pipe(jshint.reporter('jshint-stylish'))
    .pipe(jshint.reporter('fail'));
});

gulp.task('inject',function(){
    var files = gulp.src([sources.stylesheets[1],sources.stylesheets[0],sources.scripts[1]]);
    gulp.src('index.html', {cwd: './app'})
    .pipe(inject(files, {
        read: false,
        ignorePath: '/app'
    }))
    .pipe(gulp.dest('./app'));
});

gulp.task('bower',function(){
    gulp.src('./app/index.html')
    .pipe(wiredep({
        directory: './app/lib'
    }))
    .pipe(gulp.dest('./app'));
});

gulp.task('transpile-bootstrap', function(){
    return gulp.src('./app/lib/bootstrap/less/bootstrap.less')
    .pipe(less({
      paths: [ path.join(__dirname, 'less', 'includes') ]
    }))
    .pipe(gulp.dest('./app/stylesheets/css/bootstrap'));
});

gulp.task('watch',function(){
    gulp.watch(['./app/stylesheets/less/bootstrap/*.less'], ['transpile-bootstrap']);
    gulp.watch(sources.html, ['html']);
    gulp.watch(sources.stylesheets, ['css','inject']);
    gulp.watch(sources.scripts, ['jshint','inject']);
    gulp.watch(['./bower.json'],['bower']);
});

gulp.task('serve-dist',function(){
    connect.server({
        root:'./dist',
        port: 9000,
        hostname: '0.0.0.0',
        livereload: true
    });
});

gulp.task('default', ['server','inject','watch']);
