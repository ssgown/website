'use strict';

var gulp = require('gulp');
var gulpif = require('gulp-if');
var useref = require('gulp-useref');
var minifyCss = require('gulp-minify-css');
var uglify = require('gulp-uglify');

gulp.task('production', function(){
    gulp.src('./app/index.html')
    .pipe(useref())
    .pipe(gulpif('*.js', uglify({mangle:false})))
    .pipe(gulpif('*.css', minifyCss()))
    .pipe(gulp.dest('./dist'));

    gulp.src('./app/lib/font-awesome/fonts/**')
    .pipe(gulp.dest('./dist/static/fonts'));

    gulp.src('./app/static/img/**')
    .pipe(gulp.dest('./dist/static/img'));

    gulp.src('./app/favicon.ico')
    .pipe(gulp.dest('./dist/static/'));
});
